import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import {MatGridListModule} from '@angular/material/grid-list';
import { MatTabsModule } from '@angular/material';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { HelloComponent } from './hello.component';
import { FemaleMaleComponent } from './single-demographic/female-male.component';
import { DemographicHeaderComponent } from './demographic-header/demographic-header.component';
import { GridListDynamicComponent } from './gridlist-dynamic/grid-list-dynamic-example';



@NgModule({
  imports:      [ BrowserModule, BrowserAnimationsModule, FormsModule, MatTabsModule, MatGridListModule, AccordionModule.forRoot()],
  declarations: [ AppComponent, HelloComponent, FemaleMaleComponent, DemographicHeaderComponent, GridListDynamicComponent ],
  bootstrap:    [ AppComponent ],
  exports: [
       
        FormsModule,
        AccordionModule
    ]
})
export class AppModule { }
