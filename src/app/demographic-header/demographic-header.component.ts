import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-demographic-header',
    templateUrl: './demographic-header.component.html',
    styleUrls: ['./demographic-header.component.css']
})
  
export class DemographicHeaderComponent implements OnInit{

    ngOnInit(){}

    data: any = [{
        eid: 'e101',
        ename: 'Wage Grade',
        esal: 1000
      },
      {
        eid: 'e102',
        ename: 'GS/CG-01',
        esal: 2000
      },
      {
        eid: 'e103',
        ename: 'GS/CG-02',
        esal: 3000
      },
      {
        eid: 'e103',
        ename: 'GS/CG-03',
        esal: 3000
      },
      {
        eid: 'e103',
        ename: 'GS/CG-04',
        esal: 3000
      },
      {
        eid: 'e103',
        ename: 'GS/CG-05',
        esal: 3000
      },
      {
        eid: 'e103',
        ename: 'GS/CG-06',
        esal: 3000
      },
      {
        eid: 'e103',
        ename: 'GS/CG-07',
        esal: 3000
      },
      {
        eid: 'e103',
        ename: 'GS/CG-08',
        esal: 3000
      },
      {
        eid: 'e103',
        ename: 'GS/CG-09',
        esal: 3000
      },
      {
        eid: 'e103',
        ename: 'GS/CG-10',
        esal: 3000
      },
      {
        eid: 'e103',
        ename: 'GS/CG-11',
        esal: 3000
      },
      {
        eid: 'e103',
        ename: 'GS/CG-12',
        esal: 3000
      },
      {
        eid: 'e103',
        ename: 'GS/CG-13',
        esal: 3000
      },
      {
        eid: 'e103',
        ename: 'GS/CG-14',
        esal: 3000
      },
      {
        eid: 'e103',
        ename: 'GS/CG-15',
        esal: 3000
      },
      {
        eid: 'e103',
        ename: 'Senior Pay Levels',
        esal: 3000
      }];

}