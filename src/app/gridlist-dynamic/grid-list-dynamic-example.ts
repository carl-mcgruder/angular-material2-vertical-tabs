import {Component} from '@angular/core';

export interface Tile {
    color: string;
    cols: number;
    rows: number;
    text: string;
  }

  /**
 * @title Dynamic grid-list
 */
@Component({
    selector: 'grid-list-dynamic-component',
    templateUrl: 'grid-list-dynamic-example.html',
    styleUrls: ['grid-list-dynamic-example.css'],
  })
  export class GridListDynamicComponent {
    tiles: Tile[] = [
      {text: 'Hispanic or Lantio', cols: 1, rows: 1, color: 'lightblue'},
      {text: 'American Indian or Alaska Native', cols: 3, rows: 1, color: 'lightblue'},
      {text: 'Asian', cols: 1, rows: 1, color: 'lightblue'},
      {text: 'Black or African American', cols: 1, rows: 1, color: 'lightblue'},
      {text: 'Native Hawaiian/Other Pl', cols: 1, rows: 1, color: 'lightblue'},
      {text: 'White', cols: 1, rows: 1, color: 'lightblue'},
      {text: 'No RNO', cols: 1, rows: 1, color: 'lightblue'},
    ];
  }