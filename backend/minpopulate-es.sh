#!/bin/bash

echo "Creating Sample data and queries"

#Create Dimension name lookups
# Partial -- as example
curl -X POST "localhost:9200/dimensions/_doc/?pretty" -H 'Content-Type: application/json' -d'
{"name" : "Demographics", "attributes": [
  {"name": "gender", "description": "identified gender", "currentflag": true, "startdate": "20200101"},
  {"name": "hispaniclatinoflag", "description": "identify as hispanc or latino", "currentflag": true, "startdate": "20200101"},
  {"name": "disability", "description": "disability code per SF-286", "currentflag": true, "startdate": "20200101"}
  ]
}
'

#Create Demographic Dimension attribute lookup values
# Create Gender Lookup Value Index
curl -X POST "localhost:9200/_bulk?pretty" -H 'Content-Type: application/json' -d'
{ "create" : { "_index" : "lv.demographic.gender" } }
{"code" :"M", "description": "Male", "currentflag": true, "startdate": "20200101"}
{ "create" : { "_index" : "lv.demographic.gender" } }
{"code" :"F", "description": "Female", "currentflag": true, "startdate": "20200101"}
{ "create" : { "_index" : "lv.demographic.gender" } }
{"code" :"N", "description": "Non-Binary", "currentflag": true, "startdate": "20200101"}
'

# Create HispanicLatino Lookup Value Index
curl -X POST "localhost:9200/_bulk?pretty" -H 'Content-Type: application/json' -d'
{ "create" : { "_index" : "lv.demographic.hispaniclatinoflag" } }
{"code" :"Y", "description": "Yes", "currentflag": true, "startdate": "20200101"}
{ "create" : { "_index" : "lv.demographic.hispaniclatinoflag" } }
{"code" :"N", "description": "No", "currentflag": true, "startdate": "20200101"}
'

# Create Disability Lookup Value Index
#TODO: Note: this is partial for demo purpose only
curl -X POST "localhost:9200/_bulk?pretty" -H 'Content-Type: application/json' -d'
{ "create" : { "_index" : "lv.demographic.disability" } }
{"code" :"01", "description": "I do not wish to identify my disability or serious health condition", "currentflag": true, "startdate": "20200101"}
{ "create" : { "_index" : "lv.demographic.disability" } }
{"code" :"02", "description": "Developmental Disability, for example, autism spectrum disorder", "currentflag": true, "startdate": "20200101"}
{ "create" : { "_index" : "lv.demographic.disability" } }
{"code" :"05", "description": "I do not have a disability or serious health condition", "currentflag": true, "startdate": "20200101"}
'

# Create Time Dimension Lookup Values
# Create CalendarYear Lookup Value Index
curl -X POST "localhost:9200/_bulk?pretty" -H 'Content-Type: application/json' -d'
{ "create" : { "_index" : "lv.time.calendaryear" } }
{"code" :"2019", "description": "2019", "currentflag": true, "startdate": "20200101"}
{ "create" : { "_index" : "lv.demographic.calendaryear" } }
{"code" :"2020", "description": "No", "2020": true, "startdate": "20200101"}
'

# Create Organization Dimension Lookup Values
# Create OrganizationName Lookup Value Index
#TODO: Note: this is partial for demo purpose only
curl -X POST "localhost:9200/_bulk?pretty" -H 'Content-Type: application/json' -d'
{ "create" : { "_index" : "lv.organization.organizationname" } }
{"code" :"ODNI", "description": "Office of the Director of National Intelligence", "currentflag": true, "startdate": "20200101"}
{ "create" : { "_index" : "lv.organization.organizationname" } }
{"code" :"NGA", "description": "National Geospatial-Intelligence Agency", "currentflag": true, "startdate": "20200101"}
{ "create" : { "_index" : "lv.organization.organizationname" } }
{"code" :"DIA", "description": "Defense Intelligence Agency", "currentflag": true, "startdate": "20200101"}
'
# Create Pay Dimension Lookup Values
# Create PayGrade Lookup Value Index
#TODO: Note: this is partial for demo purpose only
curl -X POST "localhost:9200/_bulk?pretty" -H 'Content-Type: application/json' -d'
{ "create" : { "_index" : "lv.pay.paygrade" } }
{"code" :"GS-11", "description": "General Schedule 11", "currentflag": true, "startdate": "20200101"}
{ "create" : { "_index" : "lv.pay.paygrade" } }
{"code" :"GS-09", "description": "General Schedule 09", "currentflag": true, "startdate": "20200101"}
{ "create" : { "_index" : "lv.pay.paygrade" } }
{"code" :"SES", "description": "Senior Pay Levels", "currentflag": true, "startdate": "20200101"}
'

###Create some sample facts

##Create the nest mapping for the index

curl -X PUT "localhost:9200/headcountfacts?pretty" -H 'Content-Type: application/json' -d'
{
  "mappings": {
    "properties": {
      "dimensions": {
        "type": "nested" 
      }
    }
  }
}
'

## First set; we will do just the core required fields; assumption is we passed description vs code for dimension values
## Notes: 1. unclear of value of using 'name for dimension object...not always useful on queries if doing multi-dim query'
## 2. Unclear of performance of nested query in large scale model. ok for now. It may prove more useful to denormalize dimensions based on name to flatten

curl -X POST "localhost:9200/headcountfacts/_doc/?pretty" -H 'Content-Type: application/json' -d'
{
  "americanindiancount": 1,
  "asiancount" : 20,
  "blackcount" : 20,
  "nativehipacificcount": 1,
  "whitecount": 20,
  "dimensions" : [
    {
      "name" : "Demographics",
      "hispaniclatinoflag" : "No",
      "gender": "Female"
    },
    {
      "name" : "Time",
      "calendaryear" :  "2019"
    },
    {
      "name" : "Organization",
      "organizationname": "Defense Intelligence Agency"
    },
    {
      "name" : "Pay",
      "paygrade": "General Schedule 09"
    }
  ]
}
'
curl -X POST "localhost:9200/headcountfacts/_doc/?pretty" -H 'Content-Type: application/json' -d'
{
  "americanindiancount": 1,
  "asiancount" : 50,
  "blackcount" : 10,
  "nativehipacificcount": 1,
  "whitecount": 100,
  "dimensions" : [
    {
      "name" : "Demographics",
      "hispaniclatinoflag" : "No",
      "gender": "Male"
    },
    {
      "name" : "Time",
      "calendaryear" :  "2019"
    },
    {
      "name" : "Organization",
      "organizationname": "Defense Intelligence Agency"
    },
    {
      "name" : "Pay",
      "paygrade": "General Schedule 09"
    }
  ]
}
'
curl -X POST "localhost:9200/headcountfacts/_doc/?pretty" -H 'Content-Type: application/json' -d'
{
  "americanindiancount": 0,
  "asiancount" : 10,
  "blackcount" : 10,
  "nativehipacificcount": 0,
  "whitecount": 10,
  "dimensions" : [
    {
      "name" : "Demographics",
      "hispaniclatinoflag" : "Yes",
      "gender": "Male"
    },
    {
      "name" : "Time",
      "calendaryear" :  "2019"
    },
    {
      "name" : "Organization",
      "organizationname": "Defense Intelligence Agency"
    },
    {
      "name" : "Pay",
      "paygrade": "General Schedule 09"
    }
  ]
}
'
#Same thing, different pay PayGrade

curl -X POST "localhost:9200/headcountfacts/_doc/?pretty" -H 'Content-Type: application/json' -d'
{
  "americanindiancount": 1,
  "asiancount" : 20,
  "blackcount" : 20,
  "nativehipacificcount": 1,
  "whitecount": 20,
  "dimensions" : [
    {
      "name" : "Demographics",
      "hispaniclatinoflag" : "No",
      "gender": "Female"
    },
    {
      "name" : "Time",
      "calendaryear" :  "2019"
    },
    {
      "name" : "Organization",
      "organizationname": "Defense Intelligence Agency"
    },
    {
      "name" : "Pay",
      "paygrade": "General Schedule 11"
    }
  ]
}
'
curl -X POST "localhost:9200/headcountfacts/_doc/?pretty" -H 'Content-Type: application/json' -d'
{
  "americanindiancount": 1,
  "asiancount" : 10,
  "blackcount" : 10,
  "nativehipacificcount": 1,
  "whitecount": 70,
  "dimensions" : [
    {
      "name" : "Demographics",
      "hispaniclatinoflag" : "No",
      "gender": "Male"
    },
    {
      "name" : "Time",
      "calendaryear" :  "2019"
    },
    {
      "name" : "Organization",
      "organizationname": "Defense Intelligence Agency"
    },
    {
      "name" : "Pay",
      "paygrade": "General Schedule 11"
    }
  ]
}
'
curl -X POST "localhost:9200/headcountfacts/_doc/?pretty" -H 'Content-Type: application/json' -d'
{
  "americanindiancount": 0,
  "asiancount" : 0,
  "blackcount" : 1,
  "nativehipacificcount": 0,
  "whitecount": 10,
  "dimensions" : [
    {
      "name" : "Demographics",
      "hispaniclatinoflag" : "Yes",
      "gender": "Male"
    },
    {
      "name" : "Time",
      "calendaryear" :  "2019"
    },
    {
      "name" : "Organization",
      "organizationname": "Defense Intelligence Agency"
    },
    {
      "name" : "Pay",
      "paygrade": "General Schedule 11"
    }
  ]
}
'
#Same thing; different Organization
curl -X POST "localhost:9200/headcountfacts/_doc/?pretty" -H 'Content-Type: application/json' -d'
{
  "americanindiancount": 1,
  "asiancount" : 20,
  "blackcount" : 20,
  "nativehipacificcount": 1,
  "whitecount": 20,
  "dimensions" : [
    {
      "name" : "Demographics",
      "hispaniclatinoflag" : "No",
      "gender": "Female"
    },
    {
      "name" : "Time",
      "calendaryear" :  "2019"
    },
    {
      "name" : "Organization",
      "organizationname": "Office of the Director of National Intelligence"
    },
    {
      "name" : "Pay",
      "paygrade": "General Schedule 09"
    }
  ]
}
'
curl -X POST "localhost:9200/headcountfacts/_doc/?pretty" -H 'Content-Type: application/json' -d'
{
  "americanindiancount": 1,
  "asiancount" : 50,
  "blackcount" : 10,
  "nativehipacificcount": 1,
  "whitecount": 100,
  "dimensions" : [
    {
      "name" : "Demographics",
      "hispaniclatinoflag" : "No",
      "gender": "Male"
    },
    {
      "name" : "Time",
      "calendaryear" :  "2019"
    },
    {
      "name" : "Organization",
      "organizationname": "Office of the Director of National Intelligence"
    },
    {
      "name" : "Pay",
      "paygrade": "General Schedule 09"
    }
  ]
}
'
curl -X POST "localhost:9200/headcountfacts/_doc/?pretty" -H 'Content-Type: application/json' -d'
{
  "americanindiancount": 0,
  "asiancount" : 10,
  "blackcount" : 10,
  "nativehipacificcount": 0,
  "whitecount": 10,
  "dimensions" : [
    {
      "name" : "Demographics",
      "hispaniclatinoflag" : "Yes",
      "gender": "Male"
    },
    {
      "name" : "Time",
      "calendaryear" :  "2019"
    },
    {
      "name" : "Organization",
      "organizationname": "Office of the Director of National Intelligence"
    },
    {
      "name" : "Pay",
      "paygrade": "General Schedule 09"
    }
  ]
}
'

#Let's try a couple basic queries now

# find all the facts for females; TODO: An aggregation query could be done; have to review syntax

curl -X GET "localhost:9200/headcountfacts/_search?pretty" -H 'Content-Type: application/json' -d'
{
  "query": {
    "nested": {
      "path": "dimensions",
      "query": {
        "bool": {
          "must": [
            { "match": { "dimensions.gender":  "Female" }}
          ]
        }
      }
    }
  }
}
'

# find all the facts for DIA Females; TODO: An aggregation query could be done; have to review syntax

curl -X GET "localhost:9200/headcountfacts/_search?pretty" -H 'Content-Type: application/json' -d'
{
  "query": {
    "nested": {
      "path": "dimensions",
      "query": {
        "bool": {
          "must": [
            { "match": { "dimensions.organizationname": "Defense Intelligence Agency" }},
            { "match": { "dimensions.gender":  "Female" }} 
          ]
        }
      }
    }
  }
}
'



